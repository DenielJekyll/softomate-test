window.onload = onLoadHandler;
function onLoadHandler() {
    const siteList = document.getElementById('site-list');
    chrome.storage.local.get(["siteList"], function(storedValues) {
        if (storedValues.siteList != null)
        {
            storedValues.siteList.forEach(function (site) {
                const item = document.createElement('li');
                item.innerText = site.name;
                item.addEventListener('click', function () {
                    chrome.tabs.create({
                        url: 'http://' + site.domain,
                    });
                    return false;
                });
                siteList.appendChild(item);
            });
        }
    });
}